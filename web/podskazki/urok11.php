<?php
//фукнция для случайного числа встроена в php
//http://php.net/manual/ru/function.rand.php
echo 'первая часть подсказки. случайное число в диапазоне от 100 до 1000 включительно<br>';
echo rand(100, 1000); //открой страницу в браузере и обновляй страницу - будут случайные числа от 100 до 1000

//каждый элемент массива имеет индекс - номер элемента в массиве, например
$m = []; //создаем пустой массив
$m[] = 100; //добавляем первое значение
$m[] = 101; //добавляем второе значение
$m[] = 105; //добавляем третье значение
echo '<pre>'; //только для красоты
echo 'вторая часть подсказки. Индексы - числа в квадратных скобках<br><br>';
print_r($m);


