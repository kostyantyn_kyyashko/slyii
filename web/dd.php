<?
function factorial($n) {
    if ($n == 0 || $n == 1) {
        return 1;
    }
    return $n * factorial($n - 1);
}

echo factorial(3);

/*class Model {}
class Authors extends Model {}
class Books extends Model {}
class Biblio extends Model {
    private $authors;
    private $books;
    public function __construct(Authors $authors, Books $books){
        $this->authors = $authors;
        $this->books = $books;
    }
}*/