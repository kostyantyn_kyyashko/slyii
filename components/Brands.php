<?php
/*
* Created by Kostyantyn Kyyashko
* k.konstantin.n@gmail.com
*/
namespace app\components;

use yii\base\Component;
use app\models\Esbase;
use Yii;

class Brands extends Parsed
{

    public function __construct(Parser $parser, array $config = [])
    {
        parent::__construct($parser, $config = []);
    }

    public function url_generate (array $uniq_parameners, $page = 1)
    {
        $raw_index = ($page - 1)*10;
        $index = ($page - 1)?"&startIndex={$raw_index}":"";
        $asin = $uniq_parameners['asin'];
        return "{$this->root_url}/gp/offer-listing/$asin/ref=olp_page_21?ie=UTF8&f_all=true{$index}";
    }

    public function uniq_data_fields()
    {
        return [
            'asin',
        ];
    }

    public static function fields($content = '')
    {
        return [
            'block' => //element name of field, any - your choice
                [
                    'handler' => 'html', //handler
                    'xpath' => '//*[@id="olpOfferList"]/div/div',//
                    'out' => trim($content),
                ],
            'pages' => //element name of field, any - your choice
                [
                    'handler' => 'html', //handler
                    'xpath' => '//*[@id="olpOfferListColumn"]/div[2]/ul',//
                    'out' => trim($content),
                ],
            ];
    }

    public function brand_asins ($block)
    {
        preg_match_all('/seller\=[A-Z0-9]{5,20}\"/i', $block, $z);
        $out = [];
        foreach ($z[0] as $raw) {
            $out[] = str_replace(['seller=', '"'], '', $raw);
        }
        return $out;
    }

    private function brand_name ($block)
    {
        preg_match_all('/seller\=[A-Z0-9]{5,20}\"\>/i', $block, $z);
        $out = [];
        foreach ($z[0] as $raw) {
            $out[] = str_replace(['seller=', '"'], '', $raw);
        }
        return $out;
    }

    public function pages_count ($block)
    {
        preg_match_all('/[0-9]{1,4}<span\sclass=\"aok/i', $block, $z);
        $out = [];
        foreach ($z[0] as $raw) {
            $out[] = str_replace(['seller=', '"'], '', $raw);
        }
        return $z[0];
    }




}
