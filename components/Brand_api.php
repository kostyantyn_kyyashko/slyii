<?php
/*
* Created by Kostyantyn Kyyashko
* k.konstantin.n@gmail.com
*/
namespace app\components;

use yii\base\Component;
use app\models\Esbase;
use Yii;

class Brand_api extends Parsed
{
    public $parser;

    public function __construct(Parser $parser, array $config = [])
    {
        $this->parser = $parser;
        parent::__construct($parser, $config);
    }

    public function url_generate (array $uniq_parameners)
    {
        $seller = $uniq_parameners['seller'];
        $marketplaceID = $uniq_parameners['marketplaceID'];
        return "{$this->root_url}/sp?_encoding=UTF8&marketplaceID={$marketplaceID}&seller={$seller}";
    }

    public function uniq_data_fields()
    {
        return [
            'seller',
            'marketplaceID',
        ];
    }

    public static function fields ($content = '')
    {
        return [
            'total_count' =>
                [
                    'handler' => 'ajax',

                ],

                'business_address' =>
                [
                    'handler' => 'html',
                    'xpath' => '//*[@id="seller-profile-container"]/div[2]/div/ul/li[6]/span/ul',
                    'out' => '<ul>' . trim($content) . '</ul>',
                    'type' => 'string',
                    'index' => 'not_analyzed',
                ],
            'review_block' =>
                [
                    'handler' => 'html',
                    'xpath' => '//*[@id="feedback-content"]',
                    'out' => trim($content),
                    'type' => 'string',
                    'index' => 'not_analyzed',
                ],
        ];

    }

    public static function total_count ()
    {
        $parser = new \app\components\Parser();
        $url = "https://www.amazon.de/sp/hz/ajax/products";
        $product_search_request_data = [
            'marketplace' => static
            'seller' => 'A2WN3V7BG584EG',
            'url' => '/sp/hz/ajax/products',
            'pageSize' => 12,
            'searchKeyword' => '',
            'extraRestrictions' => [],
            'pageNumber' => "3",
        ];
        $product_search_request_data = json_encode($product_search_request_data);
        $data = [
            'marketplaceID' => 'A1PA6795UKMFR9',
            'seller' => 'A2WN3V7BG584EG',
            'productSearchRequestData' => urlencode($product_search_request_data)
        ];

        $post_fields = http_build_query($data);
        $post_fields = str_replace('%25', '%', $post_fields );
        $post_fields = str_replace('%5C', '', $post_fields );
        $post_fields = str_replace('%5B%5D', '%7B%7D', $post_fields );

        $resp = $parser->web_page_get($url, '', 1, $post_fields);
        yii::$app->debig->view((json_decode($resp['content'], 1)));

    }

    private function remove_read_less ($content)
    {
        return explode('<span class="a-declarative" data-action="reduce-text"', $content)[0];
    }


}
