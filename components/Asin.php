<?php
/*
* Created by Kostyantyn Kyyashko
* k.konstantin.n@gmail.com
*/
namespace app\components;

use yii\base\Component;
use app\models\Esbase;
use Yii;

class Asin extends Parsed
{
    public $parser;

    public function __construct(Parser $parser, array $config = [])
    {
        $this->parser = $parser;
        parent::__construct($parser, $config);
    }

    public function url_generate (array $uniq_parameners)
    {
        $merchant = $uniq_parameners['merchant'];
        $page = isset($uniq_parameners['page'])?$uniq_parameners['page']:1;
        return "{$this->root_url}/s?merchant={$merchant}&page={$page}";
    }

    public function uniq_data_fields()
    {
        return [
            'merchant',
            'page',
        ];
    }

    public function fields ($content = '')
    {
        return [];
    }

    public $block_xpath = '//*[@id="resultsCol"]';

}
