<?php
/*
* Created by Kostyantyn Kyyashko
* k.konstantin.n@gmail.com
*/
namespace app\components;

use yii\base\Component;
use app\models\Esbase;
use Yii;


class Product extends Parsed {

    public function __construct(Parser $parser, array $config = [])
    {
        parent::__construct($parser, $config = []);
    }


    /**
     * generate URL for product
     * @param $uniq_parameners
     * @return string
     */
    public function url_generate (array $uniq_parameners)
    {
        $asin = $uniq_parameners['asin'];
        return "{$this->root_url}/any/dp/$asin/";
    }
    public function uniq_data_fields()
    {
        return [
            'asin',
        ];
    }

    /**
     * main description for parser
     * description see below in first element
     * @return array
     */
    public static function fields ($content = '')
    {
        return [
            'title' => //element name of field, any - your choice
                [
                    'handler' => 'text', //handler
                    'xpath' => '//*[@id="productTitle"]',//xpath to element  //получаем текст в чистом виде
                    'out' => trim($content),
                ],
            'images' =>
                [
                    'handler' => 'html',
                    'xpath' => '//*[@id="altImages"]/ul',//забираем весь html блока с маленькими картинками
                    'out' => '<ul>' . trim($content) . '</ul>',
                ],
            'review_stars' =>
                [
                    'handler' => 'text',
                    'xpath' => '//*[@id="acrPopover"]/span[1]/a/i[1]/span',//3.3 out of 5 stars
                    'out' => floatval(trim(explode('out', $content)[0])),
                ],
            'review_qty' =>
                [
                    'handler' => 'text',
                    'xpath' => '//*[@id="acrCustomerReviewText"]', // 63 customer reviews
                    'out' => intval(trim(preg_replace('/[^\d]/', '', $content))),
                ],
            'questions' =>
                [
                    'handler' => 'text',
                    'xpath' => '//*[@id="askATFLink"]/span',// 4 answered questions
                    'out' => intval(trim(explode('answered', $content)[0])),
                ],
            'manufacturer' =>
                [
                    'handler' => 'text',
                    'xpath' => '//*[@id="brand"]', //текст, но еще есть href = URL бренда
                    'out' => trim($content),
                ],
            'brand_href' =>
                [
                    'handler' => 'attribute2',
                    'xpath' => '//*[@id="brand"]', //текст, но еще есть href = URL бренда
                    'out' => trim($content),
                ],
        /*!*/'bestseller_cat' =>
                [
                    'handler' => 'html',
                    'xpath' => '//*[@id="zeitgeistBadge_feature_div"]/div/a',//html, может не быть
                    'out' => '<a>' . trim($content) . '</a>',
                ],
            'price' =>
                [
                    'handler' => 'text',
                    'xpath' => '//*[@id="priceblock_ourprice"]',//EUR 4.99
                    'out' => self::price($content),
                ],
            'old_price' =>
                [
                    'handler' => 'text',
                    'xpath' => '//*[@id="price"]/table/tbody/tr[1]/td[2]/span',
                    'out' => trim($content),
                ],
            'availability' =>
                [
                    'handler' => 'text',
                    'xpath' => '//*[@id="availability"]/span',//In stock.
                    'out' => trim($content),
                ],

            //'estimate_delivery_time' => '',//не нашел такого блока
            'seller_shipper' =>
                [
                    'handler' => 'html',
                    'xpath' => '//*[@id="shipsFromSoldBy_feature_div"]',//html, в нем seller id="merchant-info"
                    'out' => '<div>' . trim($content) . '</div>',
                ],
            'qty_offers' =>
                [
                    'handler' => 'text',
                    'xpath' => '//*[@id="olp_feature_div"]/div/span/a',//52&nbsp;new
                    'out' => $content,
                ],
            'bullet_points' =>
                [
                    'handler' => 'html',
                    'xpath' => '//*[@id="feature-bullets"]',//html
                    'out' => trim($content),
                ],
            'product_description' =>
                [
                    'handler' => 'html',
                    'xpath' => '//*[@id="productDescription_feature_div"]',//html
                    'out' => trim($content),
                ],
            'technical_informations' =>
                [
                    'handler' => 'html',
                    'xpath' => '//*[@id="prodDetails"]/div[2]/div[1]/div/div[2]/div/div/table',//html table
                    'out' => '<table>' . trim($content) . '</table>',
                ],

            'additional_product_informations' =>
                [
                    'handler' => 'html',
                    'xpath' => '//*[@id="prodDetails"]/div[2]/div[2]/div[1]/div[2]/div/div/table',//html table
                    'out' => '<table>' . trim($content) . '</table>',
                ],
            'customer_reviews_link' =>
                [
                    'handler' => 'attribute2',
                    'xpath' => '//*[@id="summaryStars"]/a',//href нужен, это линк на все отзывы//*[@id="summaryStars"]/a
                    'out' => trim($content),
                ],
            'product_variables' =>
                [
                    'handler' => 'product_variables',
                    'xpath' => '//*[@id="twister"]',//весь блок с вариациями
                    'out' => trim($content),
                ],
            'brands_link' =>
                [
                    'handler' => 'html',
                    'xpath' => '//*[@id="mbc"]/div[5]/div/span',//
                    'out' => trim($content),
                ],

        ];
    }

    private static function price ($content)
    {
        $content = str_replace(',', '.', $content);
        return floatval(trim(str_ireplace('eur', '', $content)));
    }

    /**
     * handlers for elements. function name must be equal element name, see in product_fields_xpath()
     * must be defined ALL functions for all elements
     */

/*    private function title ($content) {
        return trim($content);
    }*/
/*    private function images ($content) {
        return '<ul>' . trim($content) . '</ul>';
    }*/
/*    private function review_stars ($content) {
        return floatval(trim(explode('out', $content)[0]));
    }*/
/*    private function review_qty ($content) {
        return intval(trim(preg_replace('/[^\d]/', '', $content)));
    }*/
/*    private function questions ($content) {
        return intval(trim(explode('answered', $content)[0]));
    }*/
/*    private function manufacturer ($content) {
        return trim($content);
    }*/
/*    private function brand_href ($content) {
        return trim($content);
    }*/
/*    private function bestseller_cat ($content) {
        return '<a>' . trim($content) . '</a>';
    }*/
/*    private function price ($content) {
        //@todo preg_replace
        return floatval(trim(str_ireplace('eur', '', $content)));
    }*/
/*    private function availability ($content) {
        return trim($content);
    }*/
/*    private function seller_shipper ($content) {
        return '<div>' . trim($content) . '</div>';
    }*/
/*    private function qty_offers ($content) {
        return intval(trim(preg_replace('/[^\d]/', '', $content)));
    }*/
/*    private function bullet_points ($content) {
        return trim($content);
    }*/
/*    private function product_description ($content) {
        return trim($content);
    }*/
/*    private function technical_informations ($content) {
        return '<table>' . trim($content) . '</table>';
    }*/
/*    private function additional_product_informations ($content) {
        return '<table>' . trim($content) . '</table>';
    }*/
/*    private function customer_reviews_link ($content) {
        return trim($content);
    }*/

    /**
     * @param $key string
     * @return mixed
     */

}