<?php
/*
* Created by Kostyantyn Kyyashko
* k.konstantin.n@gmail.com
*/
namespace app\components;

use yii\base\Component;
use app\models\Esbase;
use Yii;


class Product_api extends Parsed {

    public function __construct(Parser $parser, array $config = [])
    {
        parent::__construct($parser, $config = []);
    }


    /**
     * generate URL for product
     * @param $uniq_parameners
     * @return string
     */
    public function url_generate (array $uniq_parameners)
    {
        $asin = $uniq_parameners['asin'];
        return "{$this->root_url}/any/dp/$asin/";
    }

    public function uniq_data_fields()
    {
        return [
            'asin',
        ];
    }

    /**
     * main description for parser
     * description see below in first element
     * @return array
     */
    public static function fields ($content = '')
    {
        return [
            'title' => //element name of field, any - your choice
                [
                    'handler' => 'text', //handler
                    'xpath' => '//*[@id="productTitle"]',
                    'out' => trim($content),
                ],
            'suggested_price' => //element name of field, any - your choice
                [
                    'handler' => 'text', //handler
                    'xpath' => '//*[@id="price"]/table/tbody/tr[1]/td[2]/span',
                    'out' => trim($content),
                ],
            'our_price' => //element name of field, any - your choice
                [
                    'handler' => 'text', //handler
                    'xpath' => '//*[@id="priceblock_ourprice"]',
                    'out' => trim($content),
                ],
            'cheapest_price' => //element name of field, any - your choice
                [
                    'handler' => 'text', //handler
                    'xpath' => '//*[@id="olp_feature_div"]/div/span/span',
                    'out' => trim($content),
                ],
            'delivery_time_message' => //element name of field, any - your choice
                [
                    'handler' => 'text', //handler
                    'xpath' => '//*[@id="ddmDeliveryMessage"]/span[1]',
                    'out' => trim($content),
                ],
            'fba_fbm' => //element name of field, any - your choice
                [
                    'handler' => 'text', //handler
                    'xpath' => '//*[@id="merchant-info"]',
                    'out' => trim($content),
                ],
            'sellers' => //element name of field, any - your choice
                [
                    'handler' => 'text', //handler
                    'xpath' => '//*[@id="olp_feature_div"]/div/span/a',
                    'out' => trim($content),
                ],
        ];
    }

    private function price ($content)
    {
        $content = str_replace(',', '.', $content);
        return floatval(trim(str_ireplace('eur', '', $content)));
    }

    /**
     * handlers for elements. function name must be equal element name, see in product_fields_xpath()
     * must be defined ALL functions for all elements
     */

/*    private function title ($content) {
        return trim($content);
    }*/
/*    private function images ($content) {
        return '<ul>' . trim($content) . '</ul>';
    }*/
/*    private function review_stars ($content) {
        return floatval(trim(explode('out', $content)[0]));
    }*/
/*    private function review_qty ($content) {
        return intval(trim(preg_replace('/[^\d]/', '', $content)));
    }*/
/*    private function questions ($content) {
        return intval(trim(explode('answered', $content)[0]));
    }*/
/*    private function manufacturer ($content) {
        return trim($content);
    }*/
/*    private function brand_href ($content) {
        return trim($content);
    }*/
/*    private function bestseller_cat ($content) {
        return '<a>' . trim($content) . '</a>';
    }*/
/*    private function price ($content) {
        //@todo preg_replace
        return floatval(trim(str_ireplace('eur', '', $content)));
    }*/
/*    private function availability ($content) {
        return trim($content);
    }*/
/*    private function seller_shipper ($content) {
        return '<div>' . trim($content) . '</div>';
    }*/
/*    private function qty_offers ($content) {
        return intval(trim(preg_replace('/[^\d]/', '', $content)));
    }*/
/*    private function bullet_points ($content) {
        return trim($content);
    }*/
/*    private function product_description ($content) {
        return trim($content);
    }*/
/*    private function technical_informations ($content) {
        return '<table>' . trim($content) . '</table>';
    }*/
/*    private function additional_product_informations ($content) {
        return '<table>' . trim($content) . '</table>';
    }*/
/*    private function customer_reviews_link ($content) {
        return trim($content);
    }*/

    /**
     * @param $key string
     * @return mixed
     */

}