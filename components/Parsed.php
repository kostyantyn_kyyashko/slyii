<?php
/*
* Created by Kostyantyn Kyyashko
* k.konstantin.n@gmail.com
*/
namespace app\components;

use yii\base\Component;
use app\models\Esbase;
use Yii;

abstract class Parsed extends Component
{
    /**
     * object of Parser
     * @var Parser
     */
    public $parser;

    public $uniq_parameters;

    /**
     * Parsed constructor.
     * @param Parser $parser
     * @param array $config
     * @param array $uniq_parameters
     */
    public function __construct(Parser $parser, array $config = [], array $uniq_parameters = [])
    {
        $this->parser = $parser;
        $this->uniq_parameters = $uniq_parameters;
        parent::__construct($config);
    }

    /**
     * Root URL of service
     * @var
     */
    public $root_url = 'https://www.amazon.de';

    /**
     * Generate URL by root URL and needed GET parameters of uniq data
     * @param array $uniq_parameners
     * @return mixed
     */
    abstract function url_generate(array $uniq_parameners);

    /**
     * Field of parser. Each field contains
     * @param string $content
     */
    public static function fields($content = ''){}

    abstract function uniq_data_fields();

    public function  get_parsed_data ($key)
    {
        $handler = static::fields('')[$key]['handler'];
        if ($handler != 'ajax') {
            $xpath = static::fields('')[$key]['xpath'];
            $content = $this->parser->$handler($xpath);
            return static::fields($content)[$key]['out'];
        }
        else {
            return static::fields('')[$key]['ajax_handler'];
        }
    }

    public static function db_mapping()
    {
        $out = [];
        foreach (static::fields() as $field => $data) {
            $type = isset($data['type'])?$data['type']:'string';
            $index = isset($data['index'])?$data['index']:'not_analyzed';
            $out[$field] = [
                'type' => $type,
                'index' => $index
            ];
        }
        return $out;
    }

    public static function value_from_string ($content, $number = 0)
    {
        if ($content) {
            preg_match_all('/\d{1,10}\.{0,1}\d{0,4}/', $content, $out);
            $z =  array_values($out[0]);
            if (array_key_exists($number, $z)) return $z[$number];
        }

        return false;
    }

    /**
     * Главная ф-я парсинга. Получает данные с веб-страницы, генерируя URL по $uniq_data,
     * возвращает массив спарсенных значений полей fields, перечисленных в ф-и fields
     * @param $uniq_data
     * @param Parser $parser
     * @param Parsed $parsed_object
     * @return array
     */
    function work_parse_main($uniq_data, \app\components\Parser $parser, \app\components\Parsed $parsed_object)
    {
        $proxy = new \app\models\Proxy();
        $cookies = $parser->web_page_get($parsed_object->root_url)['cookies'];
        $proxy->increase_stat($parser->proxy, 'bad_stat');
        sleep(10);
        $url = $parsed_object->url_generate($uniq_data);
        $html = $parser->web_page_get($url, $cookies)['content'];
        $product_fields = array_keys($parsed_object::fields());
        $out = [];
        $parsed_object->parser->xpath_create($html);
        foreach ($product_fields as $key) {
            @$content = $parsed_object->get_parsed_data($key);
            $out[$key] = $content;
        }
        return $out;
    }

    public static function _mb_ucfirst($str) {
        $fc = mb_strtoupper(mb_substr($str, 0, 1));
        return $fc.mb_substr($str, 1);
    }



}
