<?php
namespace app\controllers;

use Yii;
use yii\web\Controller;

class Api3Controller extends Controller
{
    /**
     * @var string
     */
    public $layout = 'api';

    private $true_key = 'mfo304988kN';

    public function actionItems_request ()
    {
        $session_id = str_replace('.', '_', microtime(true)) . '_' . mt_rand(1000000, 2000000);
        $key = yii::$app->request->get('key');
/*        if ($key != $this->true_key){
            sleep(1);
            die();
        }*/
        //$asins = json_decode(',', $get['asins'], 1);
        $platform = yii::$app->request->get('platform');
        if (!$platform) {
            echo 'platform_not_found';
            die();
        }
        $country = yii::$app->request->get('country');
        if (!$country) {
            echo 'country_not_found';
            die();
        }
        $class = yii::$app->request->get('class');//class
        if (!$class) {
            echo 'class_not_found';
            die();
        }
        $class = "\\app\\components\\$class";
        if (!class_exists($class)) {
            echo "class $class not exists";
            die();
        }
        $bulk_source_data = yii::$app->request->get('bulk_source_data');
        if (!$bulk_source_data) {
            echo 'bulk_source_data_not_found';
            die();
        }
        $bulk_source_data = json_decode($bulk_source_data, 1);
        if (!isset($bulk_source_data[0]) || !is_array($bulk_source_data[0])) {
            echo 'bulk_source_data_not_correct';
            die();
        }
        $db = new \app\models\Query_Buffer($session_id);
        $db->fields_mapping = $class::db_mapping();
        $db->create_db_mapping_index();

        $client = new \GearmanClient();
        $client->addServer('127.0.0.1');
        $out = [];
        foreach ($bulk_source_data as $uniq_data) {
            $data_for_gearman = [
                'session_id' => $session_id,
                'uniq_data' => $uniq_data,
                'class' => $class
            ];
            $client->doBackground('api', serialize($data_for_gearman));
            $db->uniq_data_fileds_values = $uniq_data;
            $db->status_set('in_queue');
        }
        echo $session_id;
    }

    public function actionParsed_item_get ()
    {
        $session_id = yii::$app->request->get('session_id', false);
        $uniq_data = yii::$app->request->get('uniq_data', false);
        if (!$session_id || !$uniq_data) {
            echo 'bad_request';
            die();
        }
        $uniq_data = json_decode($uniq_data, 1);
        if (!isset($uniq_data[0]) || !is_array($uniq_data[0])) {
            echo 'uniq_data_not_correct';
            die();
        }
        $p2 = new \app\models\Query_Buffer($session_id);
        foreach ($uniq_data as $field => $value) {
            $p2->uniq_data_fileds_values = [$field => $value];
        }
        $out = $p2->status_get();
        echo json_encode($out);
        //yii::$app->debig->view($out);
    }


}
