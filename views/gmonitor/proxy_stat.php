<table class="table table-bordered table-striped table-hovered table-condensed" style="width: auto">
    <thead>
    <tr>
        <th>Proxy</th>
        <th>Country</th>
        <th>Call</th>
        <th>Success</th>
    </tr>
    </thead>
    <tbody>
    <? foreach ($proxy as $p): ?>
        <? if($p['bad_stat']): ?>
            <tr>
                <td><?=$p['proxy']?></td>
                <td><?=isset($p['country'])?$p['country']:''?></td>
                <td><?=$p['bad_stat']?></td>
                <td><?=$p['success_stat']?></td>
            </tr>
        <?endif;?>
    <? endforeach; ?>
    </tbody>
</table>
