<table class="table table-bordered table-striped table-hovered table-condensed" style="width: auto">
    <thead>
    <tr>
        <th>Country</th>
        <th>Proxies count</th>
    </tr>
    </thead>
    <tbody>
    <? foreach ($list as $country => $count): ?>
    <tr>
        <td><?=$country?></td>
        <td><?=$count?></td>
    </tr>
    <? endforeach; ?>
    </tbody>
</table>