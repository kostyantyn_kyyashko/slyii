<div>
    <form action="/gmonitor/api3test/" method="get">
        <label for="platform">platform
        <input type="text" name="platform" id="platform" value="Amazon" class="form-control" readonly="readonly">
        </label>
        <br>
        <label for="country">country
        <input type="text" name="country" id="country" value="DE" class="form-control" readonly="readonly">
        </label>
        <br>
        <label for="class">class
        <input type="text" name="class" id="class" value="<?=$params['class']?>" class="form-control">
        </label>
        <br>
        <label for="data">data
            <textarea type="text" name="data" id="data" value="<?=$params['data']?>" class="form-control"
                      style="height: 200px;"></textarea>
        </label>
        <br>
        <button type="submit" class="btn btn-success">Send data</button>
    </form>
</div>

