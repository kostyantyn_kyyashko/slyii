<table class="table table-bordered table-condensed table-striped">
    <thead>
        <tr>
            <th>Asin</th>
            <th>Title</th>
            <th>Images</th>
            <th>Review Stars</th>
            <th>Review Qty</th>
            <th>Questions</th>
            <th>Manufacturer</th>
            <th>Bestseller cat</th>
            <th>Actual Price</th>
            <th>Old Price</th>
            <th>Availability</th>
            <th>Seller Shipper</th>
            <th>Qty Offers</th>
            <th>Review</th>
            <th>Bullet Points</th>
            <th>Product Description</th>
            <th>Technical Informations</th>
            <th>additional_product_informations</th>
            <th>product_variables</th>
            <th>brands_link</th>
        </tr>
    </thead>
    <tbody>
        <? foreach ($products as $p): ?>
            <tr>
                <td>
                    <?=$p['asin']?>
                </td>
                <td>
                    <a href="<?=$p['url']?>">
                        <?=substr($p['title'],0,30)?>...
                    </a>
                </td>
                <td>
                    <?=$p['images']?>
                </td>
                <td>
                    <?=$p['review_stars']?>
                </td>
                <td>
                    <?=$p['review_qty']?>
                </td>
                <td>
                    <?=$p['questions']?>
                </td>
                <td>
                    <a href="https://www.amazon.de<?=$p['brand_href']?>" target="_blank">
                        <?=$p['manufacturer']?>
                    </a>
                </td>
                <td><?=$p['bestseller_cat']?></td>
                <td><?=$p['price']?></td>
                <td><?=$p['old_price']?></td>
                <td><?=$p['availability']?></td>
                <td><?=$p['seller_shipper']?></td>
                <td><?=$p['qty_offers']?></td>
                <td>
                    <a href="<?=$p['customer_reviews_link']?>" target="_blank">
                        Reviews
                    </a>
                </td>
                <td><?=$p['bullet_points']?></td>
                <td><?=$p['product_description']?></td>
                <td><?=$p['technical_informations']?></td>
                <td><?=$p['additional_product_informations']?></td>
                <td><?=$p['product_variables']?></td>
                <td><?=$p['brands_link']?></td>
            </tr>
        <? endforeach;?>
    </tbody>
    
</table>
