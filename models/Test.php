<?php
/*
* Created by Kostyantyn Kyyashko
* k.konstantin.n@gmail.com
*/
namespace app\models;

use app\components\Debig;
use yii;

class Test extends Esbase
{
    public function select_all_index ()
    {
        $t = $this->select_all_by_index('products');
        yii::$app->debig->view($t);
    }
}
