<?php
/*
* Created by Kostyantyn Kyyashko
* k.konstantin.n@gmail.com
*/
namespace app\models;

use app\components;
use yii;

/**
 * Class need for API. Api request create task on Gearman server and set status 'in_queue'.
 * Worker take task, parsing needed data, write data to DB and set status 'ok'
 * This class is storage of tasks statuses
 * Class Query_Buffer
 * @package app\models
 */
class Query_Buffer extends Esbase
{
    /**
     * @var \app\components\Parsed
     */
    public $uniq_data_fileds_values; //e.g. ['asin' => 'KMKLMKMLML']

    public function __construct($session_id)
    {
        $this->index = 'query_buffer';
        $this->type = $session_id; //session create when API request send tasks to Gearman server
        parent::__construct();
    }


    /**
     * @param $status //'in_queue', 'ok'
     * @param string $data //final data, JSON
     * @param int $counter //phoenix counter
     */
    public function status_set ($status, $data = '', $counter = 1)
    {
        $conditions = [];
        foreach ($this->uniq_data_fileds_values as $field => $value) {
            $conditions[] = [
                'match' => [
                    $field => $value,
                ],
            ];
        }
        $item = $this->select_global('id', $conditions);

        $row = [];
        if (!$item) {
            foreach ($this->uniq_data_fileds_values as $field => $value) {
                $row[$field] = $value;
            }
            $row['status'] = $status;
            $row['data'] = $data;
            $row['counter'] = $counter;
            $this->insert_row($row);
        }
        else {
            $id = array_keys($item)[0];
            //echo $id;
            $this->update_row_by_id($id, [
                'status' => $status,
                'data' => $data,
                'counter' => $counter,
            ]);
        }
    }

    /**
     * @return array
     */
    public function status_get ()
    {
        $conditions = [];
        foreach ($this->uniq_data_fileds_values as $field => $value) {
            $conditions[] = [
                'match' => [
                    $field => $value,
                ],
            ];
        }
        $item = $this->select_global('id', $conditions);
        $out = [];
        if (!$item) {
            $out['status'] = 'not_found';
        }
        else {
            $out = $item[0];
        }
        return $out;
    }
}
