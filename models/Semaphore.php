<?php
/*
* Created by Kostyantyn Kyyashko
* k.konstantin.n@gmail.com
*/
namespace app\models;

use app\components\Debig;
use yii;


class Semaphore extends Esbase
{
    public function __construct($session_id)
    {
        $this->index = 'semaphore';
        $this->type = $session_id;
        $this->fields_mapping = $this->fields_mapping();
        parent::__construct();
    }

    protected function fields_mapping ()
    {
        return [
            'uniq' => [
                'type' => 'string',
                'index' => 'not_analyzed',
            ],
            'status' => [
                'type' => 'string',
                'index' => 'not_analyzed',
            ],
            'time_stamp' => [
                'type' => 'integer'
            ],

        ];
    }

    public function status_check ($uniq)
    {
        $result = $this->select_by_field_value('uniq', $uniq, 'uniq');
        if (is_array($result) && array_key_exists($uniq, $result)) {
            return $result[$uniq]['status'];
        }
        return false;
    }

    public function status_set ($uniq, $status)
    {
        $this->insert_row([
            'uniq' => $uniq,
            'status' => $status,
            'time_stamp' => time(),
        ]);
    }

}
