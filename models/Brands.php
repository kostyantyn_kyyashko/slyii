<?php
/*
* Created by Kostyantyn Kyyashko
* k.konstantin.n@gmail.com
*/
namespace app\models;

use app\components\Debig;
use yii;

class Brands extends Esbase
{
    public function __construct($seller_id)
    {
        $this->index = 'brands';
        $this->type = $seller_id;
        $this->fields_mapping = $this->fields_mapping();
        parent::__construct();
    }

    public function fields_mapping()
    {
        return [];
    }
}