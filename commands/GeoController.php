<?php
/*
* Created by Kostyantyn Kyyashko
* k.konstantin.n@gmail.com
*/
namespace app\commands;

use yii\console\Controller;
use yii;

/**
 * Command line for proxies
 * Class GeoController
 * @package app\commands
 */
class GeoController extends Controller
{
    /**
     * Get proxiex by file, check country and insert into ES
     */
    public function actionProxy()
    {
        $proxy = new \app\models\Proxy();
        $proxy->clear_db();
        $proxies = explode(PHP_EOL, file_get_contents(yii::getAlias('@app/models/_proxy.txt')));
        $geo_service_url = 'http://ip-api.com/json/';
        foreach ($proxies as $p) {
            $ip = explode(':', $p)[0];
            $json = file_get_contents($geo_service_url . $ip);
            $country = json_decode($json, 1)['country'];
            echo $country . "\n";
            $row = [
                'proxy' => trim($p),
                'bad_stat' => 0,
                'captcha_stat' => 0,
                'success_stat' => 0,
                'country' => $country,
            ];
            $proxy->insert_row($row);
            usleep(500000);
        }
    }
}
